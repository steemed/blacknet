/*
 * Copyright (c) 2018 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.network

import kotlinx.io.core.ByteReadPacket
import kotlinx.serialization.Serializable
import mu.KotlinLogging
import ninja.blacknet.core.DataDB.Status
import ninja.blacknet.core.DataType
import ninja.blacknet.serialization.BlacknetEncoder
import ninja.blacknet.serialization.SerializableByteArray

private val logger = KotlinLogging.logger {}

@Serializable
class Data(private val list: DataList) : Packet {
    override fun serialize(): ByteReadPacket = BlacknetEncoder.toPacket(serializer(), this)

    override fun getType(): Int {
        return PacketType.Data.ordinal
    }

    override suspend fun process(connection: Connection) {
        if (list.size > DataType.MAX_DATA) {
            connection.dos("invalid Data size")
            return
        }

        val inv = InvList()

        for (i in list) {
            val type = i.first
            val bytes = i.second

            val hash = type.hash(bytes.array)

            if (!DataFetcher.fetched(hash)) {
                connection.dos("unrequested ${type.name} $hash")
                continue
            }

            val status = type.db.process(hash, bytes.array, connection)
            when (status) {
                Status.ACCEPTED -> inv.add(Pair(type, hash))
                Status.INVALID -> connection.dos("invalid ${type.name} $hash")
                Status.IN_FUTURE -> logger.info("in future ${type.name} $hash")
                Status.NOT_ON_THIS_CHAIN -> {
                    if (type == DataType.Block) ChainFetcher.offer(connection, hash)
                    else logger.info("not on this chain ${type.name} $hash")
                }
                Status.ALREADY_HAVE -> {}
            }
        }

        if (!inv.isEmpty())
            Node.broadcastInv(inv, connection)
    }
}

typealias DataList = ArrayList<Pair<DataType, SerializableByteArray>>
