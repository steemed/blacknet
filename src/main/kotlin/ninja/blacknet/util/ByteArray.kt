/*
 * Copyright (c) 2018 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.util

private val HEX_CHARS = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
private val HEX_CHARS_LOWER = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')

private fun hexIndex(element: Char): Int {
    val i = HEX_CHARS.indexOf(element)
    if (i == -1)
        return HEX_CHARS_LOWER.indexOf(element)
    return i
}

fun ByteArray.toHex(): String {
    val result = StringBuffer()

    forEach {
        val octet = it.toInt()
        val firstIndex = (octet and 0xF0).ushr(4)
        val secondIndex = octet and 0x0F
        result.append(HEX_CHARS[firstIndex])
        result.append(HEX_CHARS[secondIndex])
    }

    return result.toString()
}

fun fromHex(string: String) : ByteArray? {
    if (string.length % 2 == 1)
        return null

    val result = ByteArray(string.length / 2)

    for (i in 0 until string.length step 2) {
        val firstIndex = hexIndex(string[i])
        val secondIndex = hexIndex(string[i + 1])

        if (firstIndex == -1 || secondIndex == -1)
            return null

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun byteArrayOfInts(vararg ints: Int) = ByteArray(ints.size) { pos -> ints[pos].toByte() }

fun ByteArray.startsWith(bytes: ByteArray): Boolean {
    for (i in bytes.indices)
        if (this[i] != bytes[i])
            return false
    return true
}
